# Welcome to Discent Babylon

Discent Babylon is a fully-fledged Python3 preprocessor.
It was designed to simplify the process of getting started with computer
programming, mainly by allowing programmers to write code in their mother
tongue. The core is open for personal use.

## Features

- Translates Python-like code of any natural language into valid Python3 code
  that is executable by the standard python interpreter.
- Works on the program-level and therefore doesn't require any specific
  code editor plugin to be installed.
- Installs the `bpython` executable which just works out of the box - no 
  configuration or complicated command line management required. 
- The extensible platform interface allows you to develop your own language
  preprocessor and to benefit from community contributions
- Babylon relies purely on the Python standard library and has no external
  depencies which makes the setup go seemlessly.
- This repository's code is completely free for personal use, allowing you
  to experiment with and contribute to Discent Babylon yourself

## Personal Installation

In order to use Discent Babylon, you will need Python 3.7 or newer.  
Additionally, the `git` command line application is required.

---
**Note**

_Discent Babylon Professional_ permits non-personal use and comes with an
automatic installer that takes care for all the required programs to
be installed automatically. The one-step installation is supposed to work
just out of the box - regardless of what's already installed on your system.

---

Perform the following tasks in a Terminal, Console or Windows Command Prompt:

1. Clone Discent Babylon into your desired target location:  
   `git clone https://gitlab.com/discent/babylon-open babylon`

2. Change into the Babylon directory:  
   `cd babylon`

3. Execute `bpython` with your Python interpreter to verify it's working

## Running a Babylon script

First, navigate to your Babylon installation.

Then execute `bpython [path_to_babylon_script]` in a Terminal, Console or
Windows Command Prompt and the code is automatically being translated and executed. More customization options are available via `bpython --help` .

---
**Note**

_Discent Babylon Professional_ doesn't require navigating to your Babylon installation and running the script from there from the command line. Upon one-step setup, it adds `bpython` to your PATH, so that it's available from everywhere. Additionally, it enables you to double-click on a `.bpy` file
from the Windows Explorer. The Professional Edition also extends the Python
integrated development environment `IDLE` to detect, colorize and execute Babylon scripts. This is especially helpful for students who are just starting their computer programming journey.

---

## Licensing

Discent believes in modern and unbiased education. Hence, Discent Babylon is available for non-commercial personal use completely free of charge and with
full access to the source code.

Discent Babylon Professional comes with a great variety of built-in translation
packs (`platforms`) and professional dashboards for analyzation of your
student's progresses (_note: these features will be available out of the
box and won't require any additional setup, maintenance or general efforts_).

### Discent Babylon Application License for Personal Non-Commerical Use

The code contained in or associated with this repository is licensed under
the GPLv2 if all of the following conditions are met:

- the application is being used solely for personal non-commercial purposes
- the application is not being used for teaching an audience of one or
  multiple persons
- the application is being used by a single person to develop non-commercial
  or commercial platform packages for the original Discent Babylon software
- the application is not being used to build another application on top of it
  which doesn't meet the aforementioned conditions

### Discent Babylon Professional Application License for Extended Commercial and Non-Commercial Use

For further information and a list of the included features, please see the
Discent Babylon Pricing Page (coming soon).

### Work generated with the bpython application

The resulting Python script(s) generated using the official `bpython`
application are solely the intellectual property of the corresponding
Discent Babylon user.

Neither Discent, nor any third-party have the right to claim any
advancement for these scripts.

### Non-commercial Extensions

You are free to license and distribute non-commercial extension
packs you developed under a software license which discloses the source,
therefore contibuting to the Open Source Initiative, which allows
modification and exclusively permits non-commercial use.

We strongly suggest you to use the [Q Public License 1.0](https://tldrlegal.com/license/q-public-license-1.0-(qpl-1.0)) for this purpose as it fulfills
all of the aforementioned requirements.

### Commercial Extensions

Please contact Discent for further information on this topic.