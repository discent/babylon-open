from babylon.compat import future_fstrings

future_fstrings.register()

from bpython_main import main

if __name__ == '__main__':
    exit(main())