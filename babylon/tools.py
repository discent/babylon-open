from typing import Union, Dict, Tuple, Iterator
import tokenize
import token
import io

class BaseFilter(object):
    """ Common base class for platform filters

    Those filters are commonly used in platform scripts. They will be called
    on each token that's being parsed from the source code.
    """

    def filter(self, token_type: int, token_text: str) -> Union[str, None, Tuple[Tuple[int, str]]]:
        """ Base filter function. Throws a NotImplementedError.

        Parameters
        ----------
        token_type
            Must be one of the token types found in the 'token' module from the Python standard library
        token_text
            The token's text value

        Returns
        -------
        str
            The new token text if a successful filter action was performed
        None
            In case no filter action was performed (i.e. if no token matched the filter)
        Tuple[Tuple[int, str]]
            Multiple new resulting tokens of type (token_type, token_text)
        """
        
        raise NotImplementedError("The current platform filter has not implemented a filter function.")

class DictFilter(BaseFilter):
    """ Platform filter class which substitutes token.NAME tokens according to a Python dictionary """

    def __init__(self, replacements: Dict[str, str]) -> None:
        """ Constructs a DictFilter

        Parameters
        ----------
        replacements
            A Python dictionary mapping a source code token to its desired replacement
        """

        self.replacements = replacements
    
    def filter(self, token_type: int, token_text: str) -> Union[str, None]:
        """ Checks if the token_type is token.NAME and then substitutes it according to the DictFilter.replacements dict

        Parameters
        ----------
        token_type
            Must be one of the token types found in the 'token' module from the Python standard library
        token_text
            The token's text value
        
        Returns
        -------
        str
            The new token text if a replacement was performed
        None
            In case no replacement was performed
        """

        if token_type == token.NAME and token_text in self.replacements:
            return self.replacements[token_text]

def code_to_tokens(code: str) -> Iterator[tokenize.TokenInfo]:
    """ Converts any given valid Python 3 code into the corresponding code tokens.

    Parameters
    ----------
    code
        Any Python3 code string
    
    Returns
    -------
    Iterator[tokenize.TokenInfo]
        A generator which contains the extracted TokenInfo tokens
    """

    pseudo_source_file = io.StringIO(code)
    tokens = tokenize.generate_tokens(pseudo_source_file.readline)
    result = list()

    for token in tokens:
        result.append((token[0], token[1]))
    
    return result