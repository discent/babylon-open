""" Discent Babylon Translation Utility """

from typing import TextIO, Iterator, Tuple, Any
from babylon.tools import code_to_tokens
import itertools
import tokenize
import logging
import token
import os

class Translator(object):
    utility_location = os.path.dirname(os.path.dirname(__file__)).replace('\\', '/')
    pythonpath_tokens = code_to_tokens(f"import sys\nsys.path.append('{utility_location}')")

    def __init__(self, source_file: TextIO, source_platform: Any, license_key: str, verbose_logging: bool=None) -> None:
        """ Constructs a new Translator for exactly one source file
        
        Parameters
        ----------
        source_file
            The source script's opened file handle
        source_platform
            Python module reference from any of babylon.platforms.*
        license_key
            A valid Discent Babylon license key to sign the resulting program with#
        [verbose_logging] optional
            Whether to enable verbose logging during runtime. If None, logging won't
            be configured.
        """
        self.source_file = source_file
        self.source_platform = source_platform
        self.stdlib_dir = os.path.join(os.path.dirname(source_platform.__file__), source_platform.stdlib_dir).replace("\\", "/")

        self.license_tokens = code_to_tokens(f'import builtins\nbuiltins.__license = """{license_key}"""')
        self.pythonpath_tokens = itertools.chain(self.pythonpath_tokens, code_to_tokens(f"\nsys.path.append('{self.stdlib_dir}')"))
        self.hook_imports_tokens = code_to_tokens("from babylon.platform_tools import BabylonImportHook as __import_hook\n" + 
            f"import {source_platform.package_path} as __platform\n" +
            "sys.meta_path.insert(0, __import_hook(__platform))"
        )
        self.integration_tokens = code_to_tokens(f"from babylon import integrated as __integrated\n__integrated.report_execution(__license)")

        if verbose_logging is not None:
            if verbose_logging:
                self.logging_tokens = code_to_tokens('import logging\nlogging.basicConfig(level=logging.DEBUG, format="[%(levelname)s from Babylon] %(message)s")')
            else:
                self.logging_tokens = code_to_tokens('import logging\nlogging.basicConfig(level=logging.FATAL, format="[%(levelname)s from Babylon] %(message)s")')
        else:
            self.logging_tokens = []
    
    def translate(self) -> Iterator[Tuple[int, str]]:
        """ Translates the Translator.source_file's code into valid Python 3 tokens

        Returns
        -------
        Iterator[Tuple[int, str]]
            A generator containing Python 3 token tuples of type (token_type, token_text)
        """

        logging.debug(f"Transpiling file '{self.source_file.name}'")
        
        self.source_file.seek(0)
        for _type, text, start, end, line in tokenize.generate_tokens(self.source_file.readline):
            filtered_result = self.source_platform.platform_filter.filter(_type, text)
            if type(filtered_result) == tuple:
                for item in filtered_result:
                    yield item
            elif filtered_result is not None:
                yield _type, filtered_result
            else:
                yield _type, text
    
    def render(self, tokens: Iterator[Tuple[int, str]]) -> str:
        """ Renders Python 3 tokens to Python source code

        Parameters
        ----------
        tokens
            Iterable containing the Python token tuples of type (token_type, token_text)

        Returns
        -------
        str
            Valid Python 3 code
        """

        tokens = itertools.chain(self.hook_imports_tokens, ((token.NEWLINE, "\n"),), tokens)
        tokens = itertools.chain(self.source_platform.builtins_import_tokens, ((token.NEWLINE, "\n"),), tokens)
        tokens = itertools.chain(self.source_platform.exception_hook_tokens, ((token.NEWLINE, "\n"),), tokens)
        tokens = itertools.chain(self.integration_tokens, ((token.NEWLINE, "\n"),), tokens)
        tokens = itertools.chain(self.license_tokens, ((token.NEWLINE, "\n"),), tokens)
        tokens = itertools.chain(self.logging_tokens, ((token.NEWLINE, "\n"),), tokens)
        tokens = itertools.chain(self.pythonpath_tokens, ((token.NEWLINE, "\n"),), tokens)
        return tokenize.untokenize(tokens)
    
def translate_file(file: TextIO, platform: Any, license_key: str=None, verbose_logging: bool=None) -> None:
    """ Translates a Babylon script file using Translator().translate and generates the corresponding .py file

    Parameters
    ----------
    file
        A text file handle generated with open(...)
    platform
        Already imported platform package to be used (i.e. babylon.discent.platforms.de)
    [license_key] optional
        A valid Discent Babylon license key to sign the resulting program with. If None,
        Babylon will try to access a global built-in called "__license". If this doesn't
        exist, the Python interpreter will raise a NameError.
    [verbose_logging] optional
        Whether to enable verbose logging during runtime. If None, logging won't
        be configured.
    """

    if license_key is None:
        #noqa disables microsoft linter warnings
        license_key = __license #noqa
    script_translator = Translator(file, platform, license_key, verbose_logging)
    result_tokens = script_translator.translate()
    result_text = script_translator.render(result_tokens)
    with open(os.path.splitext(file.name)[0] + ".py", "w") as output_file:
        output_file.write(result_text)