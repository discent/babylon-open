""" Babylon<->Discent Suite integration utilities. Feel free to modify this file to your comfort for personal use. """

import urllib.request
import webbrowser
import subprocess
import threading
import platform
import builtins
import logging
import locale
import json
import math
import sys
import os

from babylon import distro
from babylon.cpuinfo import get_cpu_info

def get_manufacturer_details():
    vendor = None
    product = None
    if platform.system() == "Linux":
        if os.path.exists("/sys/devices/virtual/dmi/id/bios_vendor"):
            with open("/sys/devices/virtual/dmi/id/bios_vendor") as file:
                vendor = file.read()
        if os.path.exists("/sys/devices/virtual/dmi/id/product_name"):
            with open("/sys/devices/virtual/dmi/id/product_name") as file:
                product = file.read()
    elif platform.system() == "Darwin":
        vendor = "Apple Inc."
        product = "Macintosh"
    elif platform.system() == "Windows":
        try:
            vendor = subprocess.check_output("wmic computersystem get manufacturer", shell=True).decode("utf-8").split("\n")[1]
        except:
            logging.warning("Could not resolve Computer Vendor from WMI")
        try:
            product = subprocess.check_output("wmic computersystem get model", shell=True).decode("utf-8").split("\n")[1]
        except:
            logging.warning("Could not resolve Computer Model/Product from WMI")
    
    return vendor.rstrip("\n"), product.rstrip("\n")

def get_hardware_details():
    total_ram = None
    if platform.system() == "Linux":
        try:
            total_ram = int(subprocess.check_output("awk '/MemTotal/{print $2}' /proc/meminfo", shell=True).decode("utf-8")) * 1024
        except Exception as e:
            logging.warning("Could not resolve physical memory amount from /proc/meminfo")
    elif platform.system() == "Windows":
        try:
            total_ram = int(subprocess.check_output("wmic computersystem get TotalPhysicalMemory", shell=True).decode("utf-8").split("\n")[1])
        except:
            logging.warning("Could not resolve physical memory amount from WMI")
    else:
        try:
            total_ram = int(subprocess.check_output("sysctl -n hw.memsize", shell=True).decode("utf-8"))
        except:
            logging.warning("Could not resolve physical memory amount from sysctl")
    
    cpu = get_cpu_info()
    cpu_frequency = cpu["hz_advertised"][0]
    arch = cpu["arch"]
    bits = cpu["bits"]
    brand = cpu["brand_raw"]

    return total_ram, cpu_frequency, arch, bits, brand

def get_user_id():
    if platform.system() == "Darwin":
        uid = subprocess.check_output("""ioreg -rd1 -c IOPlatformExpertDevice | awk '/IOPlatformUUID/ { split($0, line, "\""); printf("%s\n", line[4]); }'""", shell=True).decode("utf-8").strip("\n")
    elif platform.system() == "Windows":
        uid = subprocess.check_output("wmic csproduct get uuid", shell=True).decode("utf-8").split("\n")[1]
    elif platform.system() == "Linux":
        if os.path.exists("/etc/machine-id"):
            with open("/etc/machine-id") as file:
                uid = file.read()
        else:
            print("[Error] We are sorry! Your Linux System is not capable of running Discent Babylon. We suggest you using one of the popular Linux distributions (like Ubuntu, CentOS oser SuSe). Thank you for your understanding.")
            sys.exit(1)
    else:
        print("[Error] We are sorry! Your Operating System is not capable of running Discent Babylon. We suggest you using one of the free and popular Linux distributions like Ubuntu, Debian or SuSe.")
        sys.exit(1)
    return uid.rstrip("\n")

def report_execution(_license):
    threading.Thread(target=_report_execution, args=(_license,)).start()

def _report_execution(_license):
    if hasattr(builtins, "execution_reported"):
        return
    builtins.execution_reported = True
    language = locale.getdefaultlocale()[0]
    if platform.system() == "Linux":
        os_name, os_version, _ = distro.linux_distribution()
    else:
        os_name = platform.system()
        os_version = platform.release()

    computer_vendor, computer_product = get_manufacturer_details()
    total_ram, cpu_frequency, cpu_arch, cpu_bits, cpu_brand = get_hardware_details()
    user_id = get_user_id()

    data = {
        "software": {
            "os": os_name,
            "os_family": platform.system(),
            "os_version": os_version,
            "python_version": f"{sys.version_info.major}.{sys.version_info.minor}",
            "default_browser": webbrowser.get().name
        },
        "hardware": {
            "computer_vendor": computer_vendor,
            "computer_product": computer_product,
            "total_ram": total_ram,
            "total_ram_gb": round(total_ram / (1024*1024*1024), 1),
            "cpu_frequency_ghz": round(cpu_frequency / (1000*1000*1000), 1),
            "cpu_arch": cpu_arch,
            "cpu_bits": cpu_bits,
            "cpu_brand": cpu_brand
        },
        "user": {
            "id": user_id,
            "license": _license,
            "language": language
        }
    }

    data = json.dumps(data).encode("utf8")
    request = urllib.request.Request("http://d34030dc49c3.ngrok.io/babylon/interface/enroll", data=data, headers={"content-type": "application/json"})
    try:
        response = urllib.request.urlopen(request, timeout=2)
        text = json.loads(response.read())
        if text["status"] == "ok":
            if text["message"] is not None:
                print(f"[Discent Babylon] {text['message']}")
        else:
            logging.warning(f"The Discent Babylon backend reported the non-ok status: '{text['status']}'")
    except urllib.request.URLError:
        logging.error("Could not reach Discent Babylon backend")
    except ValueError:
        logging.error("Could not read Discent Babylon backend's answer using JSON decoder")
