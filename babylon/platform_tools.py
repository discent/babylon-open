# This file is based on: https://stackoverflow.com/a/43573798

from importlib.util import spec_from_file_location
from importlib.abc import MetaPathFinder
import atexit
import sys
import os

from babylon.translator import translate_file

class BabylonImportHook(MetaPathFinder):
    def __init__(self, platform):
        self.platform = platform
        super().__init__()
    def find_spec(self, fullname, path, target=None):
        if path is None or path == "":
            #path = [os.getcwd()] # top level import -- 
            path = sys.path
        if "." in fullname:
            *parents, name = fullname.split(".")
        else:
            name = fullname

        for entry in path:
            if os.path.isdir(os.path.join(entry, name)):
                # this module has child modules
                filename = os.path.join(entry, name, "__init__.py")
                submodule_locations = [os.path.join(entry, name)]
            else:
                filename = os.path.join(entry, name + ".py")
                submodule_locations = None

            if os.path.exists(os.path.splitext(filename)[0] + ".bpy"):
                # Babylon Python File
                with open(os.path.splitext(filename)[0] + ".bpy") as source_file:
                    translate_file(source_file, self.platform)
                atexit.register(lambda: os.remove(filename))
                return
            elif os.path.exists(filename):
                # Regular Python File
                return
            else: continue
