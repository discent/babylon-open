#!/usr/bin/env python3

""" Discent Babylon is a multi-natural-language preprocessor and standard-library extension for Python. """

__copyright__ = "Copyright 2020, Discent Technologies"
__author__ = "Discent Technologies"
__license__ = "Proprietary"
__version__ = "1.0.0"
__credits__ = []

import os
import sys
import json
import logging
import argparse
import importlib
import subprocess

if sys.version_info.major < 3 or sys.version_info.major == 3 and sys.version_info.minor < 4:
    print("Discent Babylon requires at least Python 3.4 or newer to be executed. Most operating systems will support this version.", file=sys.stderr)
    sys.exit(1)

if sys.version_info.major == 3 and sys.version_info.minor < 6:
    sys.path.insert(0, os.path.join(os.path.dirname(__file__), "babylon", "compat"))

from typing import TextIO, Any

from babylon.translator import Translator, translate_file

def execute(file: TextIO) -> None:
    # -B will disable the creation of __pycache__
    python_executable = sys.executable.replace(" ", "\\ ")
    os.system(f"{python_executable} -B \"{os.path.splitext(file.name)[0] + '.py'}\"")

def cleanup(file: TextIO) -> None:
    os.remove(os.path.splitext(file.name)[0] + '.py')

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("input_file", type=argparse.FileType("r"))
    parser.add_argument("-v", "--verbose", help="increase preprocessor verbosity", action="store_true")
    parser.add_argument("-d", "--dry", help="prints the generated Python3 code to the screen and quits", action="store_true")
    args = parser.parse_args()
    
    with open(os.path.join(os.path.dirname(__file__), "config.json")) as file:
        config = json.load(file)

    log_format = "[%(levelname)s from Babylon] %(message)s"
    log_level = logging.DEBUG if args.verbose else logging.FATAL
    logging.basicConfig(level=log_level, format=log_format)
    
    if args.dry:
        script_translator = Translator(args.input_file, importlib.import_module(config["platform"]), config["license_key"], args.verbose)
        result_tokens = script_translator.translate()
        result_text = script_translator.render(result_tokens)
        print(result_text)
        exit()
    
    translate_file(args.input_file, importlib.import_module(config["platform"]), config["license_key"], args.verbose) #TODO: make this import determined by the license key and the purchased platform
    execute(args.input_file)
    cleanup(args.input_file)